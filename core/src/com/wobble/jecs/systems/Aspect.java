package com.wobble.jecs.systems;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.utils.Pool.Poolable;
import com.wobble.jecs.components.Component;
import com.wobble.jecs.components.ComponentManager;
import com.wobble.jecs.entities.Entity;

public class Aspect implements Poolable {
	private final List<Component<?>> requiredComponents;
	private final List<Component<?>> rejectedComponents;

	public Aspect() {
		this.requiredComponents = new ArrayList<Component<?>>();
		this.rejectedComponents = new ArrayList<Component<?>>();

		this.reset();
	}

	/**
	 * Checks if this is the identity `Aspect` for the `merge` operations. The identity is the same as
	 *that created by the
	 * default constructor.
	 *
	 * @return `true` if `this` is equivalent to `new Aspect()`.
	 */
	public boolean isIdentity() {
		return this.requiredComponents.isEmpty() && this.rejectedComponents.isEmpty();
	}

	@Override
	public void reset() {
		this.requiredComponents.clear();
		this.rejectedComponents.clear();
	}

	public static Aspect merge(final Aspect... aspects) {
		final Aspect mergedAspect = new Aspect();

		for (final Aspect aspect : aspects) {
			mergedAspect.requiredComponents.addAll(aspect.requiredComponents);
			mergedAspect.rejectedComponents.addAll(aspect.rejectedComponents);
		}

		return mergedAspect;
	}

	public boolean isMoreRefinedThan(final Aspect other) {
		final boolean requiredIsMoreRefined =
		    other.requiredComponents.containsAll(this.requiredComponents);
		final boolean rejectedIsMoreRefined =
		    other.rejectedComponents.containsAll(this.rejectedComponents);

		return requiredIsMoreRefined && rejectedIsMoreRefined;
	}

	public Aspect require(final Component<?>... components) {
		for (final Component<?> component : components) {
			this.requiredComponents.add(component);
		}

		return this;
	}

	public Aspect reject(final Component<?>... components) {
		for (final Component<?> component : components) {
			this.rejectedComponents.add(component);
		}

		return this;
	}

	public boolean matches(final ComponentManager componentManager, final Entity entity) {
		for (final Component<?> component : requiredComponents) {
			if (!componentManager.hasComponentFor(entity, component)) {
				return false;
			}
		}

		for (final Component<?> component : rejectedComponents) {
			if (componentManager.hasComponentFor(entity, component)) {
				return false;
			}
		}

		return true;
	}

	public boolean requires(final Component<?> component) {
		return this.requiredComponents.contains(component);
	}

	public boolean rejects(final Component<?> component) {
		return this.rejectedComponents.contains(component);
	}

	public Iterable<Component<?>> requiredComponents() {
		return this.requiredComponents();
	}
}
