package com.wobble.jecs.systems;

import com.wobble.jecs.components.AspectedComponentManager;
import com.wobble.jecs.entities.EntityManager;

public interface EntitySystem {
	public Aspect getAspect();

	public void run(float delta, EntityManager entityManager,
	                AspectedComponentManager componentManager, SystemManager systemManager);
}
