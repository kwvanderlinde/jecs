package com.wobble.jecs.systems;

import java.util.ArrayList;
import java.util.List;

import com.wobble.jecs.components.AspectedComponentManager;
import com.wobble.jecs.components.BasicAspectedComponentManager;
import com.wobble.jecs.components.ComponentManager;
import com.wobble.jecs.entities.EntityManager;

public class ConcreteSystemManager implements SystemManager {
	private final EntityManager entityManager;
	private final ComponentManager componentManager;
	// TODO Add system dependencies.
	private final List<EntitySystem> systems;

	public ConcreteSystemManager(final EntityManager entityManager,
	                             final ComponentManager componentManager) {
		this.entityManager = entityManager;
		this.componentManager = componentManager;
		this.systems = new ArrayList<EntitySystem>();
	}

	@Override
	public void addSystem(final EntitySystem system) {
		this.systems.add(system);
	}

	@Override
	public void removeSystem(final EntitySystem system) {
		this.systems.remove(system);
	}

	public void runSystems(final float delta) {
		for (final EntitySystem system : this.systems) {
			final AspectedComponentManager aspectedComponentManager =
			    new BasicAspectedComponentManager(this.componentManager, system.getAspect());

			system.run(delta, this.entityManager, aspectedComponentManager, this);
		}
	}
}
