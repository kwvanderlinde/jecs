package com.wobble.jecs.systems;

public interface SystemManager {
	public void addSystem(EntitySystem system);

	public void removeSystem(EntitySystem system);
}
