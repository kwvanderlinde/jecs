package com.wobble.jecs;

import com.wobble.jecs.components.BasicComponentManager;
import com.wobble.jecs.components.ComponentManager;
import com.wobble.jecs.entities.EntityManager;
import com.wobble.jecs.systems.ConcreteSystemManager;
import com.wobble.jecs.systems.SystemManager;

public class Context {
	private final EntityManager entityManager;
	private final ComponentManager componentManager;
	private final ConcreteSystemManager systemManager;

	public Context() {
		this.entityManager = new EntityManager();
		this.componentManager = new BasicComponentManager(this.entityManager);
		this.systemManager = new ConcreteSystemManager(this.entityManager, this.componentManager);
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public ComponentManager getComponentManager() {
		return componentManager;
	}

	public SystemManager getSystemManager() {
		return systemManager;
	}

	public void tick(final float delta) {
		this.systemManager.runSystems(delta);
	}
}
