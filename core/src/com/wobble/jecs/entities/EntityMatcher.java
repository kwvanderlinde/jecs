package com.wobble.jecs.entities;

import com.wobble.jecs.Range;
import com.wobble.jecs.systems.Aspect;

public interface EntityMatcher {
	public Range<? extends Entity> getEntities(final Aspect aspect);
}
