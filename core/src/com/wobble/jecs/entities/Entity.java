package com.wobble.jecs.entities;

import com.badlogic.gdx.utils.Pool.Poolable;

public final class Entity implements Poolable {
	int id;

	@Override
	public void reset() {
		this.id = -1;
	}

	public int getId() {
		return id;
	}
}
