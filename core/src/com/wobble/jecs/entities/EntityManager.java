package com.wobble.jecs.entities;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.badlogic.gdx.utils.Pool;
import com.wobble.jecs.Range;

public class EntityManager {
	private final Pool<Entity> entityPool;
	private final Set<Entity> liveEntities;

	private Deque<Integer> freeIds;
	private int nextId;

	public EntityManager() {
		this.entityPool = new Pool<Entity>() {
			@Override
			protected Entity newObject() {
				return new Entity();
			}
		};
		this.liveEntities = new HashSet<Entity>();

		this.freeIds = new ArrayDeque<Integer>();
		this.nextId = 0;
	}

	private int getNewIndex() {
		if (this.freeIds.isEmpty()) {
			return nextId++;
		} else {
			return freeIds.removeLast();
		}
	}

	public Entity createEntity() {
		final Entity instance = this.entityPool.obtain();

		instance.id = this.getNewIndex();

		this.liveEntities.add(instance);

		return instance;
	}

	public void destroyEntity(final Entity instance) {
		assert this.manages(instance);

		this.liveEntities.remove(instance);

		entityPool.free(instance);
	}

	public boolean manages(final Entity instance) {
		return this.liveEntities.contains(instance);
	}

	public Range<Entity> getEntities() {
		return new Range<Entity>() {
			private final Iterator<Entity> iterator;
			private Entity current;

			{
				this.iterator = EntityManager.this.liveEntities.iterator();
				this.moveToNext();
			}

			private void moveToNext() {
				if (iterator.hasNext()) {
					this.current = iterator.next();
				} else {
					this.current = null;
				}
			}

			@Override
			public void popFront() {
				this.moveToNext();
			}

			@Override
			public Entity front() {
				return current;
			}

			@Override
			public boolean empty() {
				return current == null;
			}
		};
	}
}
