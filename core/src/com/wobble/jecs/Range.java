package com.wobble.jecs;

public interface Range<T> {
	public boolean empty();

	public T front();

	public void popFront();
}
