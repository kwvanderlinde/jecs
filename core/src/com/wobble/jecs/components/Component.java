package com.wobble.jecs.components;

public interface Component<T> {
	public T createData();
}
