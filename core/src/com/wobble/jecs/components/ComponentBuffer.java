package com.wobble.jecs.components;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.wobble.jecs.Range;
import com.wobble.jecs.entities.Entity;

class ComponentBuffer<T> {
	private final Component<T> component;
	private final Map<Entity, T> data;

	public ComponentBuffer(final Component<T> component) {
		this.component = component;
		this.data = new HashMap<Entity, T>();
	}

	public T add(final Entity entity) {
		assert !this.data.containsKey(entity);

		// Attempt to create the component index.
		final T instance = component.createData();

		// Store the component data.
		this.data.put(entity, instance);

		return instance;
	}

	public T get(final Entity entity) {
		assert this.data.containsKey(entity);

		return this.data.get(entity);
	}

	public boolean contains(final Entity entity) {
		return this.data.containsKey(entity);
	}

	public void remove(final Entity entity) {
		this.data.remove(entity);
	}

	public Range<Entity> entityRange() {
		return new Range<Entity>() {
			private final Iterator<Entity> iterator;
			private Entity next;

			{
				iterator = data.keySet().iterator();

				this.moveToNext();
			}

			private void moveToNext() {
				next = iterator.hasNext() ? iterator.next() : null;
			}

			@Override
			public void popFront() {
				assert !this.empty();

				this.moveToNext();
			}

			@Override
			public Entity front() {
				assert !this.empty();

				return this.next;
			}

			@Override
			public boolean empty() {
				return this.next == null;
			}
		};
	}
}
