package com.wobble.jecs.components;

import com.wobble.jecs.Range;
import com.wobble.jecs.entities.Entity;
import com.wobble.jecs.entities.EntityManager;
import com.wobble.jecs.systems.Aspect;

public class BasicComponentManager implements ComponentManager {
	private class AspectedRange implements Range<Entity> {
		private final Aspect aspect;
		private final Range<Entity> range;

		public AspectedRange(final Aspect aspect) {
			this.aspect = aspect;

			this.range = BasicComponentManager.this.entityManager.getEntities();

			this.findNext();
		}

		private void findNext() {
			while (!this.range.empty()) {
				final Entity entity = this.range.front();

				if (this.aspect.matches(BasicComponentManager.this, entity)) {
					break;
				}

				this.range.popFront();
			}
		}

		@Override
		public boolean empty() {
			return this.range.empty();
		}

		@Override
		public Entity front() {
			assert !this.empty();

			return this.range.front();
		}

		@Override
		public void popFront() {
			assert !this.empty();

			this.range.popFront();
			this.findNext();
		}
	}

	private final EntityManager entityManager;
	private final ComponentMap componentMap;

	public BasicComponentManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
		this.componentMap = new ComponentMap();
	}

	protected <T> ComponentBuffer<T> getComponentBuffer(final Component<T> component) {
		return this.componentMap.getBuffer(component);
	}

	@Override
	public <T> T addComponentFor(final Entity entity, final Component<T> component) {
		return this.getComponentBuffer(component).add(entity);
	}

	@Override
	public <T> boolean hasComponentFor(final Entity entity, final Component<T> component) {
		return this.getComponentBuffer(component).contains(entity);
	}

	@Override
	public <T> T getComponentFor(final Entity entity, final Component<T> component) {
		return this.getComponentBuffer(component).get(entity);
	}

	@Override
	public <T> void removeComponentFor(final Entity entity, final Component<T> component) {
		this.getComponentBuffer(component).remove(entity);
	}

	@Override
	public <T> Range<Entity> getEntitiesByComponent(final Component<T> component) {
		return this.getComponentBuffer(component).entityRange();
	}

	@Override
	public Range<Entity> matchEntites(final Aspect aspect) {
		if (aspect.isIdentity()) {
			// No restrictions; enumerate all entities.
			return this.entityManager.getEntities();
		} else {
			return new AspectedRange(aspect);
		}
	}
}
