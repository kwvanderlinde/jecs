package com.wobble.jecs.components;

import com.wobble.jecs.Range;
import com.wobble.jecs.entities.Entity;
import com.wobble.jecs.systems.Aspect;

public class BasicAspectedComponentManager implements AspectedComponentManager {
	private final ComponentManager componentManager;
	private final Aspect aspect;

	public BasicAspectedComponentManager(final ComponentManager componentManager,
	                                     final Aspect aspect) {
		this.componentManager = componentManager;
		this.aspect = aspect;
	}

	@Override
	public <T> T addComponentFor(final Entity entity, final Component<T> component) {
		assert aspect.rejects(component);

		return this.componentManager.addComponentFor(entity, component);
	}

	@Override
	public <T> boolean hasComponentFor(final Entity entity, final Component<T> component) {
		return this.componentManager.hasComponentFor(entity, component);
	}

	@Override
	public <T> T getComponentFor(final Entity entity, final Component<T> component) {
		assert aspect.requires(component);

		return this.componentManager.getComponentFor(entity, component);
	}

	@Override
	public <T> void removeComponentFor(final Entity entity, final Component<T> component) {
		assert aspect.requires(component);

		this.componentManager.removeComponentFor(entity, component);
	}

	@Override
	public <T> Range<Entity> getEntitiesByComponent(final Component<T> component) {
		assert aspect.requires(component);

		return this.componentManager.getEntitiesByComponent(component);
	}

	@Override
	public Range<Entity> matchEntites(final Aspect aspect) {
		assert aspect.isMoreRefinedThan(this.aspect);

		return this.componentManager.matchEntites(aspect);
	}

	public Range<Entity> getEntities() {
		return this.matchEntites(this.aspect);
	}
}
