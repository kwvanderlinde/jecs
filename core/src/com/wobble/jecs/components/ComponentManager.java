package com.wobble.jecs.components;

import com.wobble.jecs.Range;
import com.wobble.jecs.entities.Entity;
import com.wobble.jecs.systems.Aspect;

public interface ComponentManager {
	public <T> T addComponentFor(Entity entity, Component<T> component);

	public <T> boolean hasComponentFor(Entity entity, Component<T> component);

	public <T> T getComponentFor(Entity entity, Component<T> component);

	public <T> void removeComponentFor(Entity entity, Component<T> component);

	public <T> Range<Entity> getEntitiesByComponent(Component<T> component);

	public Range<Entity> matchEntites(Aspect aspect);
}