package com.wobble.jecs.components;

import java.util.HashMap;
import java.util.Map;

import com.wobble.jecs.entities.Entity;

class ComponentMap {
	private final Map<Component<?>, ComponentBuffer<?>> componentMap;

	public ComponentMap() {
		this.componentMap = new HashMap<Component<?>, ComponentBuffer<?>>();
	}

	public <T> ComponentBuffer<T> getBuffer(final Component<T> component) {
		@SuppressWarnings("unchecked")
		ComponentBuffer<T> result = (ComponentBuffer<T>) componentMap.get(component);

		if (result == null) {
			// Do auto-insertion of a buffer.
			result = new ComponentBuffer<T>(component);
			componentMap.put(component, result);
		}

		return result;
	}

	public void remove(final Entity entity) {
		for (final ComponentBuffer<?> buffer : componentMap.values()) {
			buffer.remove(entity);
		}
	}
}
