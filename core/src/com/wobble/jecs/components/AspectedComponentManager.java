package com.wobble.jecs.components;

import com.wobble.jecs.Range;
import com.wobble.jecs.entities.Entity;

public interface AspectedComponentManager extends ComponentManager {
	public Range<Entity> getEntities();
}
