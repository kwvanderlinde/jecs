#!/bin/sh

CLANG_FORMAT="clang-format"

STAGE_FORMATTED_FILES=false

# Ask the user which files they would like formatted.
FILES=""
echo -n "Which files should be formatted [(A)ll / (M)odified / (S)taged]? "
read ams
while true; do
	case $ams in
		  [Aa]* )
          FILES="$FILES $(find . -name "*.java")";
          break;;
		  [Mm]* )
          FILES="$FILES $(git diff --name-only --diff-filter=ACMRT HEAD)";
          break;;
		  [Ss]* )
          FILES="$FILES $(git diff --cached --name-only --diff-filter=ACMRT)";
          STAGE_FORMATTED_FILES=true
          break;;
		  * )
          echo -n "Please enter one of A, M or S: ";
          read ams;;
	esac
done

for FILE in $FILES
do
    # Check the file extension
    if [ ${FILE##*.} = "java" ]
    then
        echo "Formatting $FILE"
        "$CLANG_FORMAT" -i $FILE

        # Commit the change if needed.
        if [ $STAGE_FORMATTED_FILES = true ]
        then
            git add $FILE
        fi
    fi
done
